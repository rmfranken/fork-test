/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package main

import (
	"net/http/httputil"
	"net/url"

	"github.com/labstack/echo/v4"
)

// SetProxyPrefix sets a proxy for a given prefix.
// dstScheme and dstHost are the destination scheme and host.
// It also allows to add middlewares to the prefix.
func SetProxyPrefix(e *echo.Echo, prefix string, dstScheme string, dstHost string, middlewares ...echo.MiddlewareFunc) {
	group := e.Group(prefix)

	proxy := httputil.NewSingleHostReverseProxy(&url.URL{
		Scheme: dstScheme,
		Host:   dstHost,
	})
	group.Any("*", echo.WrapHandler(proxy), middlewares...)
}
