/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package main

import (
	"context"
	"custodian/acs/eventhandler/config"
	"encoding/json"
	"errors"
	"log"

	mongofunc "gitlab.com/data-custodian/custodian-go/connectors/mongo"
	mqconnector "gitlab.com/data-custodian/custodian-go/connectors/mq"
	custodian "gitlab.com/data-custodian/custodian-go/models"
)

type revocation struct {
	ContractID   string   `json:"contract_id"`
	SignatureIDs []string `json:"signature_ids"`
}

var (
	mongoContractStrings mongofunc.MongoConnection
	mongoContracts       mongofunc.MongoConnection
	mongoSignatures      mongofunc.MongoConnection

	// When using Pulsar
	mqConsumer mqconnector.PulsarConnector

	// When using Kafka
	/*
	   mqConsumer mqconnector.KafkaConnector
	*/
)

// uploadContract creates a new contract using the fields in the `msg` variable (JSON)
func uploadContract(ctx context.Context, msg []byte) error {
	var contractString *custodian.ContractString

	// We convert the msg into a custodian.ContractString struct
	err := json.Unmarshal(msg, &contractString)
	if err != nil {
		log.Println(err.Error())
		return err
	}

	var contractBody *custodian.ContractBody
	// We convert the msg into a custodian.ContractString struct
	err = json.Unmarshal([]byte(contractString.Contract), &contractBody)
	if err != nil {
		log.Println(err.Error())
		return err
	}

	err = mongoContractStrings.AddAsset(ctx, contractString.OID, contractString)
	if err != nil {
		log.Println(err.Error())
		return err
	}

	err = mongoContracts.AddAsset(ctx, contractBody.OID, contractBody)
	if err != nil {
		log.Println(err.Error())
		return err
	}
	return nil
}

// signContract creates a new signature using the fields in the `msg` variable (JSON)
func signContract(ctx context.Context, msg []byte) error {
	var signature *custodian.Signature

	// We convert the msg into a custodian.Signature struct
	err := json.Unmarshal(msg, &signature)
	if err != nil {
		log.Println(err.Error())
		return err
	}

	err = mongoSignatures.AddAsset(ctx, signature.OID, signature)
	if err != nil {
		log.Println(err.Error())
		return err
	}
	return nil
}

// revokeConsent revokes the contract and signatures specified in the `msg` variable (JSON)
func revokeConsent(ctx context.Context, msg []byte) error {
	var revocation revocation

	// We convert the msg into a revocation struct
	err := json.Unmarshal(msg, &revocation)
	if err != nil {
		log.Println(err.Error())
		return err
	}

	if revocation.ContractID == "" {
		return errors.New("contract_id is empty")
	} else if revocation.SignatureIDs == nil {
		return errors.New("signature_ids is empty")
	}

	err = mongoContracts.DeleteAsset(ctx, revocation.ContractID)
	if err != nil {
		log.Println(err.Error())
		return err
	}

	for _, signatureID := range revocation.SignatureIDs {
		err = mongoSignatures.DeleteAsset(ctx, signatureID)
		if err != nil {
			log.Println(err.Error())
			return err
		}
	}
	return nil
}

func main() {
	ctx := context.Background()

	err := config.ReadConfig()
	if err != nil {
		log.Printf(err.Error())
		return
	}

	mongoContractStrings = mongofunc.NewMongoDB(
		config.C.MongoDB.URL,                    // MongoDB URL
		config.C.MongoDB.Name,                   // Database name
		config.C.MongoDB.Tables.ContractStrings, // Table in the database
	)

	mongoContracts = mongofunc.NewMongoDB(
		config.C.MongoDB.URL,           // MongoDB URL
		config.C.MongoDB.Name,          // Database name
		config.C.MongoDB.Tables.Bodies, // Table in the database
	)

	mongoSignatures = mongofunc.NewMongoDB(
		config.C.MongoDB.URL,               // MongoDB URL
		config.C.MongoDB.Name,              // Database name
		config.C.MongoDB.Tables.Signatures, // Table in the database
	)

	// Close the DB connections when the program exits
	defer mongoContractStrings.CloseClientDB(ctx)
	defer mongoContracts.CloseClientDB(ctx)
	defer mongoSignatures.CloseClientDB(ctx)

	mqConsumer = mqconnector.PulsarConnector{
		URL: config.C.Pulsar.URL,
	}

	// When using Kafka
	/*
	   mqConsumer = mqconnector.KafkaConnector{
	       config.C.Kafka.URL,
	       10*time.Second,
	   }
	*/

	// We first remove the data from the DB to recreate it from scratch and avoid any inconsistency
	log.Println("Removing data from the DB.")
	err = mongoContractStrings.EraseCollection(ctx)
	if err != nil {
		log.Fatalf("Could not erase collection: %v", err)
	}
	err = mongoContracts.EraseCollection(ctx)
	if err != nil {
		log.Fatalf("Could not erase collection: %v", err)
	}
	err = mongoSignatures.EraseCollection(ctx)
	if err != nil {
		log.Fatalf("Could not erase collection: %v", err)
	}

	commands := make(map[string]mqconnector.CommandHandler)

	commands[config.C.Pulsar.Keys.UploadContract] = uploadContract
	commands[config.C.Pulsar.Keys.Sign] = signContract
	commands[config.C.Pulsar.Keys.Revoke] = revokeConsent

	mqConsumer.SubscribeAndConsume(ctx, config.C.Pulsar.Topics.Contracts, config.C.Pulsar.SubscriptionNames.Contracts, commands, config.C.Pulsar.MaxRetries, config.C.Pulsar.SleepTime)

}
