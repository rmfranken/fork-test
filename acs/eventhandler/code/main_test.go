package main

import (
	"context"
	"custodian/acs/eventhandler/config"
	"strings"
	"testing"
)

func setTestConfig() {
	config.C.MongoDB.URL = "mongodb://localhost:27017"
	config.C.MongoDB.Name = "test"
	config.C.MongoDB.Tables.ContractStrings = "contract_strings"
	config.C.MongoDB.Tables.Bodies = "bodies"
	config.C.MongoDB.Tables.Signatures = "signatures"
}

var (
	contractJSON       string = `{"id":"did:custodian:MyContract","tag":[{"name":"clientid","value":"custodian-client"}],"contract":[{"lang":"en","title":"End User Agreement","description":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","check":"I agree to the terms of the service","action":["accept"]},{"lang":"ch-fr","title":"Contrat d'utilisateur","description":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","check":"J'accepte les conditions générales d'utilisation","action":["accepter"]}],"var":[{"id":"did:custodian:MyContract#userid","value":"","type":"id","regexp":"[0-9a-z:_-]+"},{"id":"did:custodian:MyContract#controllerid","value":"MontreuxJazz","type":"","regexp":""}],"subject":[{"id":"did:custodian:MyContract#subj-1","attribute":{"id":"#userid","sign":["#id"]}},{"id":"did:custodian:MyContract#subj-2","attribute":{"id":"#controllerid","sign":["#id"]}}],"verb":[{"id":"did:custodian:MyContract#deploy","function":"add","attribute":{}}],"object":[{"id":"did:custodian:MyContract#obj-1","owner":"#subj-2","attribute":{}}],"scope":[{"subject":["#subj-1","#subj-2"],"verb":["#deploy"],"object":["#obj-1"]}],"validity":{"notBefore":"2021-01-01T00:00:00Z","notAfter":"2022-12-01T23:59:59Z","sign":["#userid","#controllerid"]}}`
	contractStringJSON string = `{"id":"did:custodian:MyContract", "contract":"` + strings.Replace(contractJSON, "\"", "\\\"", -1) + `"}`
	signatureJSON      string = `{"id":"did:custodian:MySignature","header":{"body":{"id":"did:custodian:MyContract","digestMethod":"SHA256base64","digest":"FnYchy+BFVC75D8WZxcI95FMNxEXmyHOqIwSp+PFswo="},"prev":{}},"seal":[]}`
)

func TestUploadContract(t *testing.T) {
	ctx := context.Background()

	// We check that the conversion of the contract string to JSON works and don't expect the code to connect to mongodb
	err := uploadContract(ctx, []byte(contractStringJSON))
	if err.Error() != `error parsing uri: scheme must be "mongodb" or "mongodb+srv"` {
		t.Errorf("Expected error parsing uri, got %v", err)
	}
}

func TestSignContract(t *testing.T) {
	ctx := context.Background()

	// We check that the conversion of the contract string to JSON works and don't expect the code to connect to mongodb
	err := signContract(ctx, []byte(signatureJSON))
	if err.Error() != `error parsing uri: scheme must be "mongodb" or "mongodb+srv"` {
		t.Errorf("Expected error parsing uri, got %v", err)
	}
}

func TestRevokeConsent(t *testing.T) {
	ctx := context.Background()

	err := revokeConsent(ctx, []byte(`{"contractID":"did:custodian:MyContract","signatureIDs":["did:custodian:MySignature"]}`))
	if err.Error() != "contract_id is empty" {
		t.Errorf("Expected error parsing uri, got %v", err)
	}

	err = revokeConsent(ctx, []byte(`{"contract_id":"did:custodian:MyContract","signatureIDs":["did:custodian:MySignature"]}`))
	if err.Error() != "signature_ids is empty" {
		t.Errorf("Expected error parsing uri, got %v", err)
	}

	// We check that the conversion of the contract string to JSON works and don't expect the code to connect to mongodb
	err = revokeConsent(ctx, []byte(`{"contract_id":"did:custodian:MyContract","signature_ids":["did:custodian:MySignature"]}`))
	if err.Error() != `error parsing uri: scheme must be "mongodb" or "mongodb+srv"` {
		t.Errorf("Expected error parsing uri, got %v", err)
	}
}
