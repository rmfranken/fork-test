/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package config

import (
	"crypto/ecdsa"
	"strings"

	"gitlab.com/data-custodian/custodian-go/util"

	"github.com/spf13/viper"
)

type config struct {
	// Config struct for webapp config
	Server server `yaml:"Server"`

	// Config struct for PAP
	PAP server `yaml:"PAP"`

	// Config struct for EPP
	AuditTrailUrl string `yaml:"AuditTrailUrl"`

	// Config struct for IMS
	IMS server `yaml:"IMS"`

	// Config struct for the SSL files
	TLS tls `yaml:"TLS"`

	// Location of the ECDSA Private Key
	PrivateKeyLocation string `yaml:"PrivateKeyLocation"`

	// ECDSA Private Key (loaded from PrivateKeyLocation)
	PrivateKey *ecdsa.PrivateKey

	// Public key ID
	Kid string `yaml:"KID"`

	// FIXME: This should be removed and the public key should be retreived from the KMS
	// Location of the ECDSA Public Key
	CmsPublicKeyLocation string `yaml:"CmsPublicKeyLocation"`

	CmsPublicKey *ecdsa.PublicKey
}

type server struct {
	//	hostname string `yaml:"hostname"`
	Hostname string `yaml:"hostname"`
	//	host string `yaml:"port"`
	Port string `yaml:"port"`
	// URL path containing the component name (e.g., "/accesscontrol/pdp")
	ComponentUrlPath string `yaml:"ComponentUrlPath"`
}

type tls struct {
	// Path to the CA's certificate
	CaCert string `yaml:"caCert"`
	// Path to the server's certificate
	ServerCert string `yaml:"serverCert"`
	// Path to the server's private key
	ServerKey string `yaml:"serverKey"`
}

// We set a default values in case config.yml is not configured
// These default values will be replaced by values configured in config/config.yml
var C config = config{
	Server: server{"localhost", "8011", ""},
}

func ReadConfig() error {
	Config := &C
	viper.SetConfigName("config") // name of the config file
	viper.SetConfigType("yaml")
	viper.AddConfigPath("config")

	replacer := strings.NewReplacer(".", "_")
	viper.SetEnvKeyReplacer(replacer)

	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err != nil {
		return err
	}

	if err := viper.Unmarshal(&Config); err != nil {
		return err
	}

	privateKey, err := util.ReadECDSAPrivateKey(Config.PrivateKeyLocation)
	if err != nil {
		return err
	}
	Config.PrivateKey = privateKey

	cmsPublicKey, err := util.ReadECDSAPublicKey(Config.CmsPublicKeyLocation)
	if err != nil {
		return err
	}
	Config.CmsPublicKey = cmsPublicKey

	// remove after just for testing
	//spew.Dump(C)
	return nil
}
