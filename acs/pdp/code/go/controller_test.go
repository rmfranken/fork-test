package pdp

import (
	"custodian/acs/pdp/config"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"
	"time"

	models "gitlab.com/data-custodian/custodian-go/models"
	custodian "gitlab.com/data-custodian/custodian-go/util"
)

func requestTestHelper(method string, url string, handler http.HandlerFunc) *httptest.ResponseRecorder {
	req := httptest.NewRequest(method, url, nil)
	w := httptest.NewRecorder()
	handler(w, req)
	return w
}

func readBody(w *httptest.ResponseRecorder) string {
	return strings.TrimSpace(w.Body.String())
}

func checkResponse(jsonResponse string, expectedResponse string, t *testing.T) {
	var response ResponseStruct
	err := json.Unmarshal([]byte(jsonResponse), &response)
	if err != nil {
		t.Errorf("Error unmarshalling json: %s", err)
	}
	if response.Resp != expectedResponse {
		t.Errorf("Expected %s, got %s", expectedResponse, response.Resp)
	}
}

var (
	testTags      string = "%7B%20%22userid%22%3A%5B%22123%22%2C%20%22456%22%5D%2C%20%22clientid%22%3A%5B%22abc%22%5D%20%7D&"
	testContext   string = "%7B%22pep%22%3A%22def%22%2C%20%22actor%22%3A%22456%22%2C%20%22owner%22%3A%22123%22%2C%20%22scope%22%3A%20%5B%22read%22%2C%20%22write%22%5D%7D"
	testTimestamp string = "1638367491"

	contractJSON       string = `{"id":"did:custodian:MyContract","tag":[{"name":"clientid","value":"custodian-client"}],"contract":[{"lang":"en","title":"End User Agreement","description":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","check":"I agree to the terms of the service","action":["accept"]},{"lang":"ch-fr","title":"Contrat d'utilisateur","description":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","check":"J'accepte les conditions générales d'utilisation","action":["accepter"]}],"var":[{"id":"did:custodian:MyContract#userid","value":"","type":"id","regexp":"[0-9a-z:_-]+"},{"id":"did:custodian:MyContract#controllerid","value":"MontreuxJazz","type":"","regexp":""}],"subject":[{"id":"did:custodian:MyContract#subj-1","attribute":{"id":"#userid","sign":["#id"]}},{"id":"did:custodian:MyContract#subj-2","attribute":{"id":"#controllerid","sign":["#id"]}}],"verb":[{"id":"did:custodian:MyContract#deploy","function":"add","attribute":{}}],"object":[{"id":"did:custodian:MyContract#obj-1","owner":"#subj-2","attribute":{}}],"scope":[{"subject":["#subj-1","#subj-2"],"verb":["#deploy"],"object":["#obj-1"]}],"validity":{"notBefore":"2021-01-01T00:00:00Z","notAfter":"2022-12-01T23:59:59Z","sign":["#userid","#controllerid"]}}`
	contractStringJSON string = `{"id":"did:custodian:MyContract", "contract":"` + strings.Replace(contractJSON, "\"", "\\\"", -1) + `"}`
	signatureJSON      string = `{"id":"did:custodian:MySignature","header":{"body":{"id":"did:custodian:MyContract","digestMethod":"SHA256base64","digest":"FnYchy+BFVC75D8WZxcI95FMNxEXmyHOqIwSp+PFswo="},"prev":{}},"seal":[{"signee":"testuser","timestamp":"2023-01-19T16:25:54.03+01:00","source":"http://localhost","signatureMethod":"ECDSA","signature":"MEUCIQDmNtfOF0gkypHdeQ3qLurAzP67PLrKRzuD+36nvum9eAIgS982yVFlga3fDjPA7u6JrGqpIsXjrrE8sejfFGF0jTk="}]}`
	contractsTriadJSON string = `{"contract_string":` + contractStringJSON + `,"contract_body":` + contractJSON + `,"contract_signature":` + signatureJSON + `}`
)

// TODO: tests for the following functions:
// - contains (done)
// - isInTimeRange (done)
// - encodeResponse (done)
// - parameterArrayToURLString (done)
// - GetAuthorization (partially done)
// - sendErrorEvent (not testable)
// - sendEvent (done)
// - checkSubjectsApproval (done)
// - findSubjectRef (done)
// - findObject
// - getAllowedScope

func TestFindSubjectRef(t *testing.T) {
	var contract models.ContractBody
	err := json.Unmarshal([]byte(contractJSON), &contract)
	if err != nil {
		t.Errorf("Error unmarshalling json: %s", err)
	}

	contract.Var[0].Value = "testuser" // Set the user id to testuser

	subjectRef := findSubjectRef(contract, "fakeuser")
	if subjectRef != "" {
		t.Errorf("Expected empty string, got '%s'", subjectRef)
	}

	subjectRef = findSubjectRef(contract, "testuser")
	if subjectRef != "#subj-1" {
		t.Errorf("#subj-1, got '%s'", subjectRef)
	}
}

// FIXME: this should be modified once checkSubjectsApproval is fixed
func TestCheckSubjectsApproval(t *testing.T) {
	var contract models.ContractBody
	err := json.Unmarshal([]byte(contractJSON), &contract)
	if err != nil {
		t.Errorf("Error unmarshalling json: %s", err)
	}

	contract.Var[0].Value = "testuser"       // Set the user id to testuser
	contract.Var[1].Value = "testcontroller" // Set the controller id to testcontroller

	// checkSubjectsApproval(contractBody custodian.ContractBody, contractSignature custodian.Signature)

	// FIXME: this should be modified once checkSubjectsApproval is fixed
	var signature models.Signature

	isValid := checkSubjectsApproval(contract, signature)
	if isValid {
		t.Errorf("Expected false, got true")
	}

	signature.Seal = append(signature.Seal, models.Seal{"testuser", time.Now(), "", "", ""})

	isValid = checkSubjectsApproval(contract, signature)
	if isValid {
		t.Errorf("Expected false, got true")
	}

	signature.Seal = append(signature.Seal, models.Seal{"testcontroller", time.Now(), "", "", ""})

	isValid = checkSubjectsApproval(contract, signature)
	if !isValid {
		t.Errorf("Expected true, got false")
	}

}

func setTestConfig(t *testing.T) {
	privateKey, err := custodian.NewECDSAKey()
	if err != nil {
		t.Error(err)
	}
	cmsPrivateKey, err := custodian.NewECDSAKey()
	if err != nil {
		t.Error(err)
	}
	config.C.CmsPublicKey = &cmsPrivateKey.PublicKey
	config.C.PrivateKey = privateKey
}

func TestContains(t *testing.T) {
	list := []string{"a", "b", "c"}
	if !contains(list, "a") {
		t.Errorf("Expected true, got false")
	}
	if contains(list, "d") {
		t.Errorf("Expected false, got true")
	}
}

func TestIsInTimeRange(t *testing.T) {
	var contractBody models.ContractBody
	err := json.Unmarshal([]byte(contractJSON), &contractBody)
	if err != nil {
		t.Errorf("Error unmarshalling json: %s", err)
	}

	// "notBefore":"2021-01-01T00:00:00Z","notAfter":"2022-12-01T23:59:59Z"

	before, err := time.Parse(time.RFC3339, "2019-12-31T23:59:59Z")
	if err != nil {
		t.Errorf("Error parsing time: %s", err)
	}

	after, err := time.Parse(time.RFC3339, "2023-01-01T00:00:00Z")
	if err != nil {
		t.Errorf("Error parsing time: %s", err)
	}

	inTimeRange, err := time.Parse(time.RFC3339, "2022-01-01T00:00:00Z")
	if err != nil {
		t.Errorf("Error parsing time: %s", err)
	}

	// test timestamp before notBefore
	if isInTimeRange(contractBody, before) {
		t.Errorf("Expected false, got true")
	}

	// test timestamp after notAfter
	if isInTimeRange(contractBody, after) {
		t.Errorf("Expected false, got true")
	}

	// test timestamp in range
	if !isInTimeRange(contractBody, inTimeRange) {
		t.Errorf("Expected true, got false")
	}
}

func TestEncodeResponse(t *testing.T) {
	expected := "{\"resp\":\"test\"}"
	w := httptest.NewRecorder()
	encodeResponse(w, "test", http.StatusOK)
	if w.Code != http.StatusOK {
		t.Errorf("Expected %d, got %d", http.StatusOK, w.Code)
	}

	if strings.TrimSpace(w.Body.String()) != expected {
		t.Errorf("Expected %s, got %s", expected, w.Body.String())
	}
}

func TestParameterArrayToURLString(t *testing.T) {
	parameter := "test"
	array := []string{"a", "b", "c"}
	expected := "test=a&test=b&test=c"
	result := parameterArrayToURLString(parameter, array)
	if result != expected {
		t.Errorf("Expected %s, got %s", expected, result)
	}
}

func TestGetAuthorization(t *testing.T) {
	setTestConfig(t)
	// test PAP that found the contract
	testPAP := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var papResponse PAPResponse
		papResponse.Resp = "ok"

		var contractsTrias ContractsTriad
		err := json.Unmarshal([]byte(contractsTriadJSON), &contractsTrias)
		if err != nil {
			t.Errorf("Error unmarshalling json: %s", err)
		}

		papResponse.ContractsTriad = contractsTrias

		jsonTriad, err := json.Marshal(papResponse)
		if err != nil {
			t.Errorf("Error marshalling json: %s", err)
		}
		fmt.Fprintf(w, "%s", jsonTriad)
	}))
	defer testPAP.Close()

	// test PAP that did not find the contract
	testPAPNotFound := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var papResponse PAPResponse
		papResponse.Resp = "not found"
		jsonTriad, err := json.Marshal(papResponse)
		if err != nil {
			t.Errorf("Error marshalling json: %s", err)
		}
		fmt.Fprintf(w, "%s", jsonTriad)
	}))
	defer testPAPNotFound.Close()

	testPAPUrl, err := url.Parse(testPAP.URL)
	if err != nil {
		t.Errorf("Error parsing url: %s", err)
	}

	testPAPNotFoundUrl, err := url.Parse(testPAPNotFound.URL)
	if err != nil {
		t.Errorf("Error parsing url: %s", err)
	}

	w := requestTestHelper("GET", "/authorization", GetAuthorization)
	checkResponse(readBody(w), "Missing 't' parameter (tags)", t)

	w = requestTestHelper("GET", "/authorization?t="+testTags, GetAuthorization)
	checkResponse(readBody(w), "Missing 'q' parameter (context)", t)

	w = requestTestHelper("GET", "/authorization?t="+testTags+"&q="+testContext, GetAuthorization)
	checkResponse(readBody(w), "Missing 'u' parameter (unix time)", t)

	config.C.PAP.Hostname = testPAPNotFoundUrl.Hostname()
	config.C.PAP.Port = testPAPNotFoundUrl.Port()

	w = requestTestHelper("GET", "/authorization?t="+testTags+"&q="+testContext+"&u="+testTimestamp, GetAuthorization)
	checkResponse(readBody(w), "Contract not found", t)

	config.C.PAP.Hostname = testPAPUrl.Hostname()
	config.C.PAP.Port = testPAPUrl.Port()

	w = requestTestHelper("GET", "/authorization?t="+testTags+"&q="+testContext+"&u="+testTimestamp, GetAuthorization)
	checkResponse(readBody(w), "Signature not valid", t)

}

func TestSendEvent(t *testing.T) {
	testEventServerOK := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	}))
	defer testEventServerOK.Close()

	testEventServerWrong := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadRequest)
	}))
	defer testEventServerWrong.Close()

	config.C.AuditTrailUrl = testEventServerWrong.URL

	err := sendEvent(0, "", "", "", nil)
	if err == nil {
		t.Errorf("Expected an error, got nil")
	}

	config.C.AuditTrailUrl = testEventServerOK.URL

	err = sendEvent(0, "", "", "", nil)
	if err != nil {
		t.Errorf("Expected nil, got %s", err)
	}

}
