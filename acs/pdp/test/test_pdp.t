test no_oid:
  $ RESP=$(curl -s --show-error "$PDP_HOST:$PDP_PORT")
  $ echo $RESP
  { "resp": "OID not valid" }

test no_fields:
  $ RESP=$(curl -s --show-error "$PDP_HOST:$PDP_PORT?oid=abc")
  $ echo $RESP
  { "resp": "Fields not valid" }

test get:
  $ RESP=$(curl -s --show-error "$PDP_HOST:$PDP_PORT?oid=abc&fields=a&fields=b&fields=c")
  $ echo $RESP
  { "resp": "Author OID not valid" }

test reply:
  $ curl -s --show-error -o /dev/null  -w "%{http_code}" "$PDP_HOST:$PDP_PORT?oid=abc&fields=a&consent_author_oid"
  200 (no-eol)
