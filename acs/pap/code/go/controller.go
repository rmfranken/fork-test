/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package pap

import (
	"context"
	"custodian/acs/pap/config"
	"encoding/json"
	"errors"
	mongofunc "gitlab.com/data-custodian/custodian-go/connectors/mongo"
	"gitlab.com/data-custodian/custodian-go/models"
	"log"
	"net/http"
	"net/url"

	"go.mongodb.org/mongo-driver/bson"
)

type ResponseStruct struct {
	Resp      string         `json:"resp"`
	Contracts ContractsTriad `json:"contracts"`
}

// ContractsTriad contains a contract signature and the contract it makes reference to, as well as the string version of the contract
type ContractsTriad struct {
	ContractString custodian.ContractString `json:"contract_string"`
	ContractBody   custodian.ContractBody   `json:"contract_body"`
	Signature      custodian.Signature      `json:"contract_signature"`
}

type tags map[string][]string

// findContractBody queries the database and returns a contract. It uses `tags` to find the contract.
func findContractBody(ctx context.Context, tags tags) (custodian.ContractBody, error) {

	mongoContracts := mongofunc.NewMongoDB(
		config.C.MongoDB.URL,
		config.C.MongoDB.Name,
		config.C.MongoDB.Tables.Bodies,
	)

	defer mongoContracts.CloseClientDB(ctx)

	// GET contract by field and contractAuthorOID

	tagsList := []bson.M{}

	for key, element := range tags {
		for _, value := range element {
			tagsList = append(tagsList, bson.M{"tag.name": key, "tag.value": value})
		}
	}

	filter := bson.M{"$and": tagsList} // FIXME: hardwired ?

	log.Println("Search contract in DB with the following filter:", filter)

	//FIXME: filter by action? add other filters?

	cursor, err := mongoContracts.GetAssetsByFilterSorted(ctx, filter, "_id", -1)
	if err != nil {
		return custodian.ContractBody{}, err
	}

	var contracts []custodian.ContractBody
	if err = cursor.All(ctx, &contracts); err != nil {
		return custodian.ContractBody{}, err
	}
	if len(contracts) > 0 {
		return contracts[0], nil // We return the first signature (sorted by insertion time in reverse order)
	} else {
		return custodian.ContractBody{}, errors.New("Contract body not found")
	}

}

// findContractString retrieves the string format of a contract from the database using its contract ID.
func findContractString(ctx context.Context, contractOID string) (custodian.ContractString, error) {

	mongoContracts := mongofunc.NewMongoDB(
		config.C.MongoDB.URL,
		config.C.MongoDB.Name,
		config.C.MongoDB.Tables.ContractStrings,
	)

	defer mongoContracts.CloseClientDB(ctx)

	var contractString custodian.ContractString

	err := mongoContracts.GetAsset(ctx, contractOID, &contractString)
	if err != nil {
		return custodian.ContractString{}, err
	}

	return contractString, nil
}

// findSignature finds and returns the last signature of a specific contract that contains at least the signatures of the users in `userOIDs`.
//
//   - `contractOID` is the ID of the contract the signature must point to.
//   - `userOIDs` is a list of user IDs. All users in the list must be present in the `seal.signee` field of the contract signature.
func findSignature(ctx context.Context, contractOID string, userOIDs []string) (custodian.Signature, error) {
	mongoAuthorizations := mongofunc.NewMongoDB(
		config.C.MongoDB.URL,
		config.C.MongoDB.Name,
		config.C.MongoDB.Tables.Signatures,
	)

	defer mongoAuthorizations.CloseClientDB(ctx)

	var conditions []bson.M

	conditions = append(conditions, bson.M{"header.body.id": contractOID})

	for _, userOID := range userOIDs {
		conditions = append(conditions, bson.M{"seal": bson.M{"$elemMatch": bson.M{"signee": bson.M{"$regex": userOID + "(\\?kid=[a-zA-Z0-9=]+)?"}}}})
	}

	filter := bson.M{"$and": conditions} // In case ?kid was not specified // FIXME: should we support more parameters?

	log.Println("Search signature in DB with the following filter:", filter)

	cursor, err := mongoAuthorizations.GetAssetsByFilterSorted(ctx, filter, "_id", -1)
	if err != nil {
		return custodian.Signature{}, err
	}

	var signatures []custodian.Signature
	if err = cursor.All(ctx, &signatures); err != nil {
		return custodian.Signature{}, err
	}
	if len(signatures) > 0 {
		return signatures[0], nil // We return the first signature (sorted by insertion time in reverse order)
	} else {
		return custodian.Signature{}, errors.New("Signature not found")
	}
}

// GetTriad returns a contract triad (the contract in its JSON and string format, and its signature JSON object) that correspond to the tags passed as URL query parameters.
func GetTriad(w http.ResponseWriter, r *http.Request) {
	// get the user's OID
	ctx := r.Context()

	tQuery, ok := r.URL.Query()["t"]
	if !ok {
		encodeResponse(w, "Tags not valid", ContractsTriad{}, http.StatusBadRequest)
		return
	}

	decodedTags, err := url.QueryUnescape(tQuery[0])
	if err != nil {
		encodeResponse(w, err.Error(), ContractsTriad{}, http.StatusBadRequest)
		return
	}

	var tags tags
	err = json.Unmarshal([]byte(decodedTags), &tags)
	if err != nil {
		encodeResponse(w, err.Error(), ContractsTriad{}, http.StatusBadRequest)
		return
	}

	// We check that tags contain the necessary fields
	if _, ok := tags["userid"]; !ok {
		encodeResponse(w, "Error: missing user ID", ContractsTriad{}, http.StatusBadRequest)
		return
	} else if _, ok := tags["clientid"]; !ok {
		encodeResponse(w, "Error: missing client ID", ContractsTriad{}, http.StatusBadRequest)
		return
	}

	log.Println("Looking for tags:", tags)

	// We get the contract body (filtered using the `tags`)
	contractBody, err := findContractBody(ctx, tags)
	if err != nil {
		log.Println("Error during contract lookup:", err.Error())
		encodeResponse(w, err.Error(), ContractsTriad{}, http.StatusNotFound)
		return
	}

	log.Println("Found contract", contractBody.OID)

	// We get the contract body stored as string
	contractString, err := findContractString(ctx, contractBody.OID)
	if err != nil {
		log.Println("Error during contract string lookup:", err.Error())
		encodeResponse(w, err.Error(), ContractsTriad{}, http.StatusNotFound)
		return
	}

	// We get the contract signature
	signature, err := findSignature(ctx, contractString.OID, tags["userid"])
	if err != nil {
		log.Println("Error during signature lookup:", err.Error())
		encodeResponse(w, err.Error(), ContractsTriad{}, http.StatusNotFound)
		return
	}

	log.Println("Found contract signature", signature.OID)

	contractsTriad := ContractsTriad{contractString, contractBody, signature}

	// encode contracts triad in the response
	encodeResponse(w, "ok", contractsTriad, http.StatusOK)

}

// encodeResponse encodes `text` and contractsTriad in json using the ResponseStruct structure, and sends it to the response writer with the status code `statusCode`.
func encodeResponse(w http.ResponseWriter, text string, contractsTriad ContractsTriad, statusCode int) {
	w.WriteHeader(statusCode)
	data := ResponseStruct{text, contractsTriad}
	e := json.NewEncoder(w)
	e.SetIndent("", "  ")
	e.Encode(data)
}
