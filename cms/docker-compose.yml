version: '3.2'
services:
    cms-contract-endpoint:
        image: $CI__REGISTRY_IMAGE_SUFFIX/cms-contract-endpoint:$CI__IMAGE_TAG
        build:
            context: ../
            dockerfile: cms/contract-endpoint/Dockerfile
            args:
                CI__REGISTRY_IMAGE: $CI__REGISTRY_IMAGE
                CI__GO_IMAGE: $CI__GO_IMAGE
                CI__IMAGE_TAG: $CI__IMAGE_TAG
                CI__BASE_TAG: $CI__BASE_TAG
                CI__CONTAINER_IMAGE: $CI__CONTAINER_IMAGE
        #expose:
        #    - 8003
        ports:
            - "8003:8003"
        environment:
            - PULSAR_URL=pulsar://platform-pulsar6650:6650
            - SERVER_HOSTNAME=0.0.0.0
            - SERVER_PORT=8003
            - SERVER_COMPONENTURLPATH=/cms
        labels:
            kompose.service.expose: ${CI__PREFIX}${CI__CUSTODIAN_DOMAIN}/cms/contract
            kompose.service.expose.tls-secret: tls-swissdatacustodian-with-ca-2023
            #kompose.service.type: nodeport
        extra_hosts:
            - "platform-pulsar6650:172.17.0.1"

    cms-contract-cmdhandler:
        image: $CI__REGISTRY_IMAGE_SUFFIX/cms-contract-cmdhandler:$CI__IMAGE_TAG
        build:
            context: ../
            dockerfile: cms/contract-cmdhandler/Dockerfile
            args:
                CI__REGISTRY_IMAGE: $CI__REGISTRY_IMAGE
                CI__GO_IMAGE: $CI__GO_IMAGE
                CI__IMAGE_TAG: $CI__IMAGE_TAG
                CI__BASE_TAG: $CI__BASE_TAG
                CI__CONTAINER_IMAGE: $CI__CONTAINER_IMAGE
        environment:
            - PULSARINPUT_URL=pulsar://platform-pulsar6650:6650
            - PULSARINPUT_MAXRETRIES=10
            - PULSARINPUT_SLEEPTIME=15s
            - PULSAROUTPUT_URL=pulsar://platform-pulsar6650:6650
            - MONGODB_URL=mongodb://platform-mongodb27017:27017
        ## FIXME: ref. https://github.com/tiangolo/full-stack-fastapi-postgresql/issues/273
        ##        depends_on:
        ##            - "platform-pulsar6650"
        ##            - "platform-mongodb27017"
        extra_hosts:
            - "platform-mongodb27017:172.17.0.1"
            - "platform-pulsar6650:172.17.0.1"

    cms-signature-endpoint:
        image: $CI__REGISTRY_IMAGE_SUFFIX/cms-signature-endpoint:$CI__IMAGE_TAG
        build:
            context: ../
            dockerfile: cms/signature-endpoint/Dockerfile
            args:
                CI__REGISTRY_IMAGE: $CI__REGISTRY_IMAGE
                CI__GO_IMAGE: $CI__GO_IMAGE
                CI__IMAGE_TAG: $CI__IMAGE_TAG
                CI__BASE_TAG: $CI__BASE_TAG
                CI__CONTAINER_IMAGE: $CI__CONTAINER_IMAGE
        #expose:
        #    - 8004
        ports:
            - "8004:8004"
        environment:
            - PULSAR_URL=pulsar://platform-pulsar6650:6650
            - SERVER_HOSTNAME=0.0.0.0
            - SERVER_PORT=8004
            - SERVER_COMPONENTURLPATH=/cms
        labels:
            kompose.service.expose: ${CI__PREFIX}${CI__CUSTODIAN_DOMAIN}/cms/signature
            kompose.service.expose.tls-secret: tls-swissdatacustodian-with-ca-2023
            #kompose.service.type: nodeport
        extra_hosts:
            - "platform-pulsar6650:172.17.0.1"

    cms-signature-cmdhandler:
        image: $CI__REGISTRY_IMAGE_SUFFIX/cms-signature-cmdhandler:$CI__IMAGE_TAG
        build:
            context: ../
            dockerfile: cms/signature-cmdhandler/Dockerfile
            args:
                CI__REGISTRY_IMAGE: $CI__REGISTRY_IMAGE
                CI__GO_IMAGE: $CI__GO_IMAGE
                CI__IMAGE_TAG: $CI__IMAGE_TAG
                CI__BASE_TAG: $CI__BASE_TAG
                CI__CONTAINER_IMAGE: $CI__CONTAINER_IMAGE
        environment:
            - PULSARINPUT_URL=pulsar://platform-pulsar6650:6650
            - PULSARINPUT_MAXRETRIES=10
            - PULSARINPUT_SLEEPTIME=15s
            - PULSAROUTPUT_URL=pulsar://platform-pulsar6650:6650
            - AUDITTRAILURL=http://accesscontrol-epp:8015/epp
            - MONGODB_URL=mongodb://platform-mongodb27017:27017
        secrets:
            - source: cms-ecdsa-private
              target: ecdsa/cms_private.pem
        extra_hosts:
            - "platform-mongodb27017:172.17.0.1"
            - "platform-pulsar6650:172.17.0.1"

    cms-status-handler:
        image: $CI__REGISTRY_IMAGE_SUFFIX/cms-status-handler:$CI__IMAGE_TAG
        build:
            context: ../
            dockerfile: cms/status-handler/Dockerfile
            args:
                CI__REGISTRY_IMAGE: $CI__REGISTRY_IMAGE
                CI__GO_IMAGE: $CI__GO_IMAGE
                CI__IMAGE_TAG: $CI__IMAGE_TAG
                CI__BASE_TAG: $CI__BASE_TAG
                CI__CONTAINER_IMAGE: $CI__CONTAINER_IMAGE
        #expose:
        #    - 8016
        ports:
            - "8016:8016"
        environment:
            - PULSARINPUT_URL=pulsar://platform-pulsar6650:6650
            - SERVER_HOSTNAME=0.0.0.0
            - SERVER_PORT=8016
            - SERVER_COMPONENTURLPATH=/cms
        labels:
            kompose.service.expose: ${CI__PREFIX}${CI__CUSTODIAN_DOMAIN}/cms/status
            kompose.service.expose.tls-secret: tls-swissdatacustodian-with-ca-2023
            #kompose.service.type: nodeport
        extra_hosts:
            - "platform-pulsar6650:172.17.0.1"

    did-resolver:
        image: $CI__REGISTRY_IMAGE_SUFFIX/did-resolver:$CI__IMAGE_TAG
        build:
            context: ../
            dockerfile: cms/did-resolver/Dockerfile
            args:
                CI__REGISTRY_IMAGE: $CI__REGISTRY_IMAGE
                CI__GO_IMAGE: $CI__GO_IMAGE
                CI__IMAGE_TAG: $CI__IMAGE_TAG
                CI__BASE_TAG: $CI__BASE_TAG
                CI__CONTAINER_IMAGE: $CI__CONTAINER_IMAGE
        #expose:
        #    - 8020
        ports:
            - "8020:8020"
        environment:
            - SERVER_HOSTNAME=0.0.0.0
            - SERVER_PORT=8020
            - SERVER_COMPONENTURLPATH=/did
            - MONGODB_URL=mongodb://platform-mongodb27017:27017
        labels:
            kompose.service.expose: ${CI__PREFIX}${CI__CUSTODIAN_DOMAIN}/did
            kompose.service.expose.tls-secret: tls-swissdatacustodian-with-ca-2023
            ## FIXME: nodePort?
        extra_hosts:
            - "platform-mongodb27017:172.17.0.1"

##FIXME: add tests to verify that secrets are correctly passed
secrets:
    cms-ecdsa-private:
        file: ${CI__SRC_PATH:-.}/custodian-internal/secrets/cms/signature/ecdsa/private.pem

#    ui-ecdsa-private:
#        file: ${CI__SRC_PATH:-.}/custodian-internal/secrets/mjf/website/ecdsa/private.pem
#    ui-keycloak-clientsecret:
#        file: ${CI__SRC_PATH:-.}/custodian-internal/credentials/__SECRET_MJF_RENKU_PEP_CLIENT_SECRET
