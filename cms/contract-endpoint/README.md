# Contract Webservice

This REST API can be used to add a new consent, modify an existing one or delete it.

The webservice receives requests and passes them in the messages queue.

## Installation

### The Dependencies

The code has been built with `Go 1.19.2`, but should work with any version that supports Go modules.

To install the dependencies, open a terminal in the `code` folder and type

```
go get
```

The contract webservice uses either Pulsar or Kafka as message queue. Make sure that the message queue you configured the code for (default is Pulsar) is currently running before running the code.

### Configuration

The config file can be found in `config/config.yml`. For the new configuration to take effect, you must rerun the service.

### Build and Run

To build and run the code, open a terminal in the `custodian/cms/contract-endpoint` folder and run the following command:
```
go build -o contract-endpoint code/main.go
./contract-endpoint
```

## Usage

The list of endpoints and their description can be found in `api/openapi.yml`

## Documentation

To read the documentation you must install `godoc`. Open a terminal in the `custodian/cms/contract-endpoint` folder or one of its subfolders, and run the following command:

```
godoc -http=localhost:6060
```

The documentation for the structures and interfaces is accessible at [http://localhost:6060/pkg/custodian/cms/contract-endpoint/code/controller/](http://localhost:6060/pkg/custodian/cms/contract-endpoint/code/controller/)
