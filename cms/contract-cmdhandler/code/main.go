/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package main

import (
	"context"
	"custodian/cms/contract-cmdhandler/config"
	"encoding/json"
	"log"
	"time"

	mongofunc "gitlab.com/data-custodian/custodian-go/connectors/mongo"
	mqconnector "gitlab.com/data-custodian/custodian-go/connectors/mq"
	custodian "gitlab.com/data-custodian/custodian-go/models"

	"github.com/apache/pulsar-client-go/pulsar"
)

type ErrorMessage struct {
	Error    string                   `json:"error"`
	Contract custodian.ContractString `json:"contract"`
}

var (
	mongoDid       mongofunc.MongoConnection
	mongoContracts mongofunc.MongoConnection

	// To use Pulsar
	mqProducer mqconnector.PulsarConnector

	mqConsumer mqconnector.PulsarConnector

	// To use Kafka
	/*
		mqProducer mqconnector.KafkaConnector

		mqConsumer mqconnector.KafkaConnector
	*/
)

// produceMessage sends a message to the "contract" topic
func produceMessage(ctx context.Context, message []byte, key string) {
	err := mqProducer.SendMessage(ctx, message, key, config.C.PulsarOutput.Topics.Contracts, nil)
	if err != nil {
		log.Fatal("Error: ", err.Error())
	}
}

// statusMessage sends a message to the "status" topic
func statusMessage(ctx context.Context, message string, key string, requestID string) {
	properties := make(map[string]string)
	properties["requestID"] = requestID
	err := mqProducer.SendMessage(ctx, []byte(message), key, config.C.PulsarOutput.Topics.Status, properties)
	if err != nil {
		log.Fatal("Error: ", err.Error())
	}
}

// Custom functions for the contracts

// uploadContract uploads a new contract file to the DB/Message Queue
func uploadContract(ctx context.Context, msg []byte, requestID string) {
	// We prepare the string version of the contract to store it
	contractBody := new(custodian.ContractBody)
	//contractString.Contract = fmt.Sprintf("%s", msg)
	err := json.Unmarshal(msg, &contractBody)
	if err != nil {
		log.Println(err.Error())
		return
	}

	contractString := new(custodian.ContractString)
	contractString.OID = contractBody.OID
	contractString.Contract = string(msg)

	log.Println("New contract", contractString.OID, "\n", contractString.Contract)

	// TODO: check if the ID matches the hash of the contract

	// We check if the OID already exists
	exists, err := mongoDid.Exists(ctx, contractString.OID)
	if err != nil {
		statusMessage(ctx, "Error: "+err.Error(), config.C.PulsarOutput.Keys.CreateContract, requestID)
		return
	} else if exists {
		log.Println("Contract already exists")
		statusMessage(ctx, "Error: OID already exists", config.C.PulsarOutput.Keys.CreateContract, requestID)
		return
	}

	// TODO: Verify the contract format

	// Store the contract in DIDs database
	err = mongoDid.AddAsset(ctx, contractString.OID, contractString)
	if err != nil {
		statusMessage(ctx, "Error: "+err.Error(), config.C.PulsarOutput.Keys.CreateContract, requestID)
		return
	}

	// Also store the contract in CMS's view
	err = mongoContracts.AddAsset(ctx, contractBody.OID, contractBody)
	if err != nil {
		statusMessage(ctx, "Error: "+err.Error(), config.C.PulsarOutput.Keys.CreateContract, requestID)
		return
	}

	newMsg, err := json.Marshal(contractString)
	if err != nil {
		statusMessage(ctx, "Error: "+err.Error(), config.C.PulsarOutput.Keys.CreateContract, requestID)
		return
	}

	// Add to MQ
	log.Printf("Message for MQ: %s", newMsg)
	produceMessage(ctx, newMsg, config.C.PulsarOutput.Keys.UploadContract)

	statusMessage(ctx, "Contract uploaded.", config.C.PulsarOutput.Keys.CreateContract, requestID)
}

func main() {
	ctx := context.Background()

	err := config.ReadConfig()
	if err != nil {
		log.Printf(err.Error())
		return
	}

	mongoDid = mongofunc.NewMongoDB(
		config.C.MongoDB.URL,
		config.C.MongoDB.Name,
		config.C.MongoDB.Tables.Dids,
	)
	mongoContracts = mongofunc.NewMongoDB(
		config.C.MongoDB.URL,
		config.C.MongoDB.Name,
		config.C.MongoDB.Tables.Contracts,
	)

	defer mongoContracts.CloseClientDB(ctx)
	defer mongoDid.CloseClientDB(ctx)

	// To use Pulsar
	mqProducer = mqconnector.PulsarConnector{
		URL: config.C.PulsarOutput.URL,
	}

	/*
		mqConsumer = mqconnector.PulsarConnector{
			URL:              config.C.PulsarInput.URL,
			ConsumeAndDelete: true,
		}
		// Maps the functions to the commands keys
		commands := make(map[string]mqconnector.CommandHandler)

		commands[config.C.PulsarInput.Keys.UploadContract] = uploadContract

		// Select different topic for Contracts creation
		mqConsumer.SubscribeAndConsume(ctx, config.C.PulsarInput.Topics.Contracts, "my-subscription-1", commands, config.C.PulsarInput.MaxRetries, config.C.PulsarInput.SleepTime)
	*/

	// To use Kafka
	/*
			mqProducer = mqconnector.KafkaConnector{
			    config.C.KafkaInput.URL,
			    10*time.Second,
			}

		    mqConsumer = mqconnector.KafkaConnector{
		        config.C.KafkaOutput.URL,
		        10*time.Second,
		    }
	*/

	msgChannel := make(chan pulsar.ConsumerMessage)

	client, err := pulsar.NewClient(pulsar.ClientOptions{
		URL: config.C.PulsarInput.URL,
	})

	if err != nil {
		log.Fatalf("Could not create client: %v", err)
	}

	pulsarConfig := pulsar.ConsumerOptions{
		Topic:            config.C.PulsarInput.Topics.Contracts,
		SubscriptionName: config.C.PulsarInput.SubscriptionName,
		Type:             pulsar.Exclusive,
		MessageChannel:   msgChannel,
	}

	var consumer pulsar.Consumer = nil

	for i := 0; i < config.C.PulsarInput.MaxRetries; i++ {
		consumer, err = client.Subscribe(pulsarConfig)

		if err != nil {
			log.Println("Could not establish subscription. Retrying in " + config.C.PulsarInput.SleepTime.String())
			time.Sleep(config.C.PulsarInput.SleepTime)
		} else {
			break
		}
	}

	if consumer == nil {
		log.Fatalf("Could not establish subscription: %v", err)
	}

	defer consumer.Close()

	for cm := range msgChannel {
		msg := cm.Message

		// Process the message
		log.Println(msg.Key(), msg.Payload(), msg.Properties())

		// Save file in destination folder

		requestID, ok := msg.Properties()["requestID"]
		if !ok {
			log.Println("No message ID")
		} else {
			uploadContract(ctx, msg.Payload(), requestID)
		}

		// To remove the message from the MQ
		consumer.Ack(msg) // FIXME: do we want to keep the messages?

	}

}
