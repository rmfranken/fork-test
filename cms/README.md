# Consent Management Engine (cms)

This folder contains the services handling consents.

* contract-endpoint: REST API to create of remove a consent template
* contract-cmdhandler: command handler for the creation and deletion of consent templates
* signature-endpoint: REST API to upload signed an existing consent template
* signature-cmdhandler: command handler that verifies the signature an stores the signed consent

# How to Run the Service With Docker Compose

Before running docker-compose, you must configure the environment variables in the `docker-compose.yml` file.

Specify the following environment variables for the contract-endpoint:
* `SERVER_HOSTNAME`: the hostname to be used by the contract-endpoint
* `SERVER_PORT`: the port the contract-endpoint listens to
* `PULSAR_URL`: the full URL of the Pulsar instance to use


Specify the following environment variables for the contract-cmdhandler:
* `PULSARINPUT_URL`: the full URL of the Pulsar instance to use as input (incoming messages)
* `PULSAROUTPUT_URL`: the full URL of the Pulsar instance to use as output (outgoing messages)
* `MONGODB_URL`: the full URL of the Mongo DB

Specify the following environment variables for the signature-endpoint:
* `SERVER_HOSTNAME`: the hostname to be used by the signature-endpoint
* `SERVER_PORT`: the port the signature-endpoint listens to
* `PULSAR_URL`: the full URL of the Pulsar instance to use


Specify the following environment variables for the signature-cmdhandler:
* `PULSARINPUT_URL`: the full URL of the Pulsar instance to use as input (incoming messages)
* `PULSAROUTPUT_URL`: the full URL of the Pulsar instance to use as output (outgoing messages)
* `MONGODB_URL`: the full URL of the Mongo DB

Create a folder with the secret files and reference them in the `docker-compose.yml` file.

Then, you can run the following command in a terminal, from the repository containing the `docker-compose.yml` file:

```
docker compose up
```
