/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cms

import (
	"context"
	"custodian/cms/status-handler/config"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/apache/pulsar-client-go/pulsar"
)

var (
	Status map[string]string
)

// ReadMessages subscribes to the status message queue and stores the status in the local variable `status`
func ReadMessages(ctx context.Context) {
	msgChannel := make(chan pulsar.ConsumerMessage)

	client, err := pulsar.NewClient(pulsar.ClientOptions{
		URL: config.C.PulsarInput.URL,
	})

	if err != nil {
		log.Fatalf("Could not create client: %v", err)
	}

	pulsarConfig := pulsar.ConsumerOptions{
		Topic:            config.C.PulsarInput.Topics.Status,
		SubscriptionName: config.C.PulsarInput.SubscriptionName,
		Type:             pulsar.Exclusive,
		MessageChannel:   msgChannel,
	}

	var consumer pulsar.Consumer = nil

	for i := 0; i < config.C.PulsarInput.MaxRetries; i++ {
		consumer, err = client.Subscribe(pulsarConfig)

		if err != nil {
			log.Println("Could not establish subscription. Retrying in " + config.C.PulsarInput.SleepTime.String())
			time.Sleep(config.C.PulsarInput.SleepTime)
		} else {
			break
		}
	}

	if consumer == nil {
		log.Fatalf("Could not establish subscription: %v", err)
	}

	defer consumer.Close()

	for cm := range msgChannel {
		msg := cm.Message

		// Process the message
		log.Println("New event:", msg.Key())
		log.Println("Payload:", msg.Payload())
		log.Println("Properties:", msg.Properties())

		// Save file in destination folder

		requestID, ok := msg.Properties()["requestID"]
		if !ok {
			log.Println("Message is missing a request ID")
		} else {
			Status[requestID] = string(msg.Payload())
		}
	}
}

// GetStatus writes the status that corresponds to the request `requestid`
func GetStatus(w http.ResponseWriter, r *http.Request) {
	requestID, ok := r.URL.Query()["requestid"]
	if !ok {
		http.Error(w, "Missing request ID", http.StatusBadRequest)
		return
	}
	if message, ok := Status[requestID[0]]; ok {
		fmt.Fprintln(w, message)
		return
	} else {
		http.Error(w, "Request ID not found.", http.StatusNotFound)
		return
	}
}
