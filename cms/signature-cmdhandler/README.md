# Contract Signatures Command Handler

The contract signatures command handler reads the signatures message queue, executes the commands, and adds the signatures to a mongo database. 

## Installation

### The Dependencies

The code has been built with `Go 1.19.2`, but should work with any version that supports Go modules.

To install the dependencies, open a terminal in the `code` folder and type

```
go get
```

The signature command handler uses either Pulsar or Kafka as message queue. Make sure that the message queue you configured the code for (default is Pulsar) is currently running before running the code.

The same applies for MongoDB: the Mongo DB instance you configured your code for must be running.

### Configuration

The config file can be found in `config/config.yml`. For the new configuration to take effect, you must rerun the service.

### Build and Run

To build and run the code, open a terminal in the `custodian/cms/signature-cmdhandler` folder and run the following command:
```
go build -o signature-cmdhandler code/main.go
./signature-cmdhandler
```
