{{/*
Expand the name of the chart.
*/}}
{{- define "custodian-audit-trail.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "custodian-audit-trail.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "custodian-audit-trail.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "custodian-audit-trail.labels" -}}
helm.sh/chart: {{ include "custodian-audit-trail.chart" . }}
{{ include "custodian-audit-trail.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "custodian-audit-trail.selectorLabels" -}}
app.kubernetes.io/name: {{ include "custodian-audit-trail.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "custodian-audit-trail.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "custodian-audit-trail.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Create the pulsar URL
*/}}
{{- define "custodian.pulsarUrl" -}}
{{- if .Values.pulsar.url }}
{{- .Values.pulsar.url }}
{{- else -}}
pulsar://{{ .Release.Name }}-custodian-pulsar-broker:6650
{{- end }}
{{- end }}

{{/*
Create the mongodb URL
*/}}
{{- define "custodian.mongodbUrl" -}}
{{- if .Values.mongodb.url }}
{{- .Values.mongodb.url }}
{{- else -}}
mongodb://{{ .Release.Name }}-custodian-mongodb:27017
{{- end }}
{{- end }}