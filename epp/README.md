# Events Processing Point

* EPP: sends new events to the message queue
* events-endpoint: REST endpoint to retrieve the events of a specific user

# How to Run the Service With Docker Compose

Before running docker compose, you must configure the environment variables in the `docker-compose.yml` file.

Create a folder with the secret files and reference them in the `docker-compose.yml` file.

Then, you can run the following command in a terminal:

```
docker compose up --build
```
