/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package epp

import (
	"gitlab.com/data-custodian/custodian-go/models"
	"encoding/json"
	"fmt"
	"net/http"
	"sort"
	"strconv"
)

var (
	Events map[string][]custodian.Event
)

// clamp makes sure that `value` stays in the [min; max] range
//
// Examples:
//   - clamp(10, 15, 20) -> 15
//   - clamp(16, 15, 20) -> 16
//   - clamp(34, 15, 20) -> 20
func clamp(value, min, max int) int {
	if value > max {
		return max
	} else if value < min {
		return min
	} else {
		return value
	}
}

// setPaginationHeaders sets the headers fields for pagination.
func setPaginationHeaders(w http.ResponseWriter, r *http.Request, page int, perPage int, nbEvents int) {
	if perPage < 1 {
		return
	}

	lastPage := int((nbEvents-1)/perPage) + 1

	w.Header().Add("x-page", fmt.Sprintf("%d", page))
	w.Header().Add("x-per-page", fmt.Sprintf("%d", perPage))
	w.Header().Add("x-total", fmt.Sprintf("%d", nbEvents))
	w.Header().Add("x-total-pages", fmt.Sprintf("%d", lastPage))

	if nbEvents < 1 {
		return
	}
	if page > 1 {
		w.Header().Add("x-prev-page", fmt.Sprintf("%d", page-1))
	}
	if page < lastPage {
		w.Header().Add("x-next-page", fmt.Sprintf("%d", page+1))
	}
	return
}

// GetEvents returns a JSON list that contains the events corresponding to the user `userid`.
func GetEvents(w http.ResponseWriter, r *http.Request) {
	userID, ok := r.URL.Query()["userid"]
	if !ok {
		http.Error(w, "Missing user ID", http.StatusBadRequest)
		return
	}

	if Events, ok := Events[userID[0]]; ok {
		// TODO: check that the user is authorized to see userid's events

		eventsPage := append(make([]custodian.Event, 0, len(Events)), Events...) // deep copy of Events
		nbEvents := len(Events)

		// We sort the events from most recent to oldest
		sort.Slice(eventsPage, func(i, j int) bool {
			return eventsPage[i].Unix > eventsPage[j].Unix
		})

		// If the parameter page is passed, we split the list using per_page (set as 10 by default)
		if pageParam, ok := r.URL.Query()["page"]; ok {
			page, err := strconv.Atoi(pageParam[0])
			if err != nil {
				http.Error(w, "Error: invalid value for `page` parameter", http.StatusBadRequest)
				return
			}
			if page < 1 {
				http.Error(w, "Error: `page` must be a positive integer", http.StatusBadRequest)
				return
			}
			perPage := 10
			if per_page, ok := r.URL.Query()["per_page"]; ok {
				perPage, err = strconv.Atoi(per_page[0])
				if err != nil {
					http.Error(w, "Error: invalid value for `per_page` parameter", http.StatusBadRequest)
					return
				} else if perPage < 1 {
					http.Error(w, "Error: `per_page` must be a positive integer", http.StatusBadRequest)
					return
				}
			}

			if (page-1)*perPage >= len(eventsPage) {
				http.Error(w, "Error: page not found", http.StatusNotFound)
				return
			}

			start := clamp((page-1)*perPage, 0, len(eventsPage)-1)
			stop := clamp(page*perPage, start, len(eventsPage))

			eventsPage = eventsPage[start:stop]

			setPaginationHeaders(w, r, page, perPage, nbEvents) // Set Header pagination parameters (see https://docs.gitlab.com/ee/api/#pagination)
		}

		jsonEvents, err := json.Marshal(eventsPage)
		if err != nil {
			http.Error(w, "Cannot marshal events", http.StatusInternalServerError)
			return
		}

		w.Header().Add("Content-Type", "application/json")
		fmt.Fprintf(w, "%s", jsonEvents)

		return
	} else {
		http.Error(w, "User ID not found.", http.StatusNotFound)
		return
	}
}
