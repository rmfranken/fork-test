CRAM=cram
DC=/usr/bin/docker-compose ## FIXME: to force usage of dc 1.27.2 while fixing kompose
KOMPOSE=kompose
KOMPOSE_PARAMS="-omanifests/"
KUBECTL_BIN=/bin/kubectl
KUBE_NAMESPACE=$(K8S_NS)
TEST_OPTS="-v"
SED_INPLACE=-i
WORKDIR=./tmp
PRINT = @echo -e "\e[1;34m Executing Makefile rule: $@\e[0m"
PRINTOK = echo -e "\e[1;34m OK $@\e[0m"
CI__KU_VERSION=v1.22.1

## this is to be used when everything else breaks, to debug the status of an ongoing CI/CD operation
debug_info::
	$(PRINT)
	env|sort|egrep '(^CI__|^CI_|^GITLAB)'
	date; uname -a
	free; df -h
	cat /etc/*release

debug_info_k8s::
	$(PRINT)
	curl -LO https://storage.googleapis.com/kubernetes-release/release/${CI__KU_VERSION}/bin/linux/amd64/kubectl && chmod +x ./kubectl
	./kubectl version --client
	## the following commands are suffixed with `|| true`, due to cicd user (i.e. it's normal if they fail under gitlab-runner)
	./kubectl cluster-info                       || true
	./kubectl get pods -n $KUBE_NAMESPACE -owide || true

docker_compose_install::
	$(PRINT)
	apt-get update
	apt-get install -y curl jq python3 python3-dev python3-pip build-essential libffi-dev libssl-dev gettext
	pip3 install docker-compose

docker_compose_convert::
	$(PRINT)
	## FIXME: https://stackoverflow.com/questions/4247068/sed-command-with-i-option-failing-on-mac-but-works-on-linux
	sed $(SED_INPLACE) 's/\([a-z-]\+\)\([[:digit:]]\+\):\([[:digit:]]\+\)/\1\2:\2/g'  docker-compose.yml
	sed $(SED_INPLACE) 's/=externalpdp/=accesscontrol-pdp/g'                          docker-compose.yml
	sed $(SED_INPLACE) 's/cmemongodb/platform-mongodb27017/g'                         docker-compose.yml

deploy_top_k8s::
	$(PRINT)
	## FIXME: this works, but it is crude; find a more generic approach, via `kubectl -k` or `$(DC) -f`
	echo ==== Rsyncing to $(WORKDIR)
	rsync -Rrl --exclude ".git" . $(WORKDIR)
	echo ==== Will now execute compose/convert/deploy makefile rules
	#[ $(CI_NOKC) ] || $(MAKE)        kompose_convert kubectl_deploys -C $(WORKDIR)/demo/kc              ## Make this conditional, upon custodian-* k8s namespaces
	$(MAKE) docker_compose_convert kompose_convert kubectl_deploys -C $(WORKDIR)/acs/
	$(MAKE) docker_compose_convert kompose_convert kubectl_deploys -C $(WORKDIR)/cms/
	$(MAKE) docker_compose_convert kompose_convert kubectl_deploys -C $(WORKDIR)/epp/
	$(MAKE) docker_compose_convert kompose_convert kubectl_deploys -C $(WORKDIR)/platform/
	#$(MAKE) docker_compose_convert kompose_convert kubectl_deploys -C $(WORKDIR)/CMS/
	#$(MAKE) docker_compose_convert kompose_convert kubectl_deploys -C $(WORKDIR)/IMS/
	$(MAKE) docker_compose_convert kompose_convert kubectl_deploys -C $(WORKDIR)/kms/
	$(MAKE) docker_compose_convert kompose_convert kubectl_deploys -C $(WORKDIR)/swagger/
	echo ==== Will now execute rm -fr $(WORKDIR)
	rm -fr $(WORKDIR) ## FIXME: there is a potential for an unsafe $(WORKDIR) value here; perhaps, use `apply -k` to avoid touching the manifests, at all.

kompose_convert::
	$(PRINT)
	$(KOMPOSE) version
	mkdir -p ./manifests
	$(DC) config | tee docker-compose-resolved.yaml && $(PRINTOK) && $(KOMPOSE) convert -f docker-compose-resolved.yaml $(KOMPOSE_PARAMS)
	## grep -rli "networking.k8s.io/v1" . | xargs -i@ sed -i "s|networking.k8s.io/v1|networking.k8s.io/v1beta1|g" @ ## FIXME: hack while waiting for a k8s upgrade on the runner
	grep -rli "$(CI__CUSTODIAN_DOMAIN)/[a-z-]\+" . | grep "ingress.yaml" | xargs -i@ sed -i "s|$(CI__CUSTODIAN_DOMAIN)/[a-z-]\+|$(CI__CUSTODIAN_DOMAIN)|g" @ ## FIXME: replace this line with a better approach (kustomize?)

kubectl_deploys::
	$(PRINT)
	$(KUBECTL_BIN) -n $(KUBE_NAMESPACE) apply -f manifests/

all:: kompose_convert kubectl_deploys ## you MUST run also: `make build-push` or the equivalent `docker-compose` commands

build-push::
	$(PRINT)
	$(DC) build --parallel
	$(DC) push

deploy:: all build-push

clean::
	$(PRINT)
	rm {core,POC}/*/manifests/*{-deployment,-service}.yaml  || true
	rmdir {core,POC}/*/manifests/				 			|| true

deploy_top_dc::
	$(PRINT)
	$(MAKE) deploy -C ./acs/
	$(MAKE) deploy -C ./cms/
	#$(MAKE) deploy -C ./CMS/
	#$(MAKE) deploy -C ./IMS/

deploy_test_top::
	$(PRINT)
##	$(MAKE) deploy_test -C demo/kc/
##	$(MAKE) deploy_test -C acs/ ## FIXME: tests not implemented yet
##	$(MAKE) deploy_test -C cms/           ## FIXME: tests not implemented yet
##	$(MAKE) deploy_test -C CMS/           ## FIXME: CMS changed, previous tests are outdated
##	$(MAKE) deploy_test -C IMS/           ## FIXME: tests not implemented yet

deploy_test::
	$(PRINT)
	$(CRAM) tests/cramtests/*.t

create_ns::
	$(PRINT)
	$(KUBECTL_BIN) create ns $(KUBE_NAMESPACE)

delete_ns::
	$(PRINT)
	$(KUBECTL_BIN) delete ns $(KUBE_NAMESPACE) || true

wait::
	$(PRINT)
	/bin/echo -n  ; sleep 1 ; /bin/echo -n  ; sleep 1 ; /bin/echo -n ; sleep 30 ## namespace generation probably justifies the delay

up::	create_ns wait deploy_top_dc

full::	delete_ns up ## FIXME: this should rebuild the world

test::
	$(PRINT)
	@date
	## FIXME: this is the port-forwarding based way of testing, deprecated by a native testing process
	@DYNAMICO_READ_HOST=localhost DYNAMICO_READ_PORT=8001 DYNAMICO_CMD_HOST=localhost DYNAMICO_CMD_PORT=8000 PAP_HOST=localhost PAP_PORT=8012 PDP_HOST=localhost PDP_PORT=8011 CONSENT_HOST=localhost CONSENT_PORT=8003 SIGNING_HOST=localhost SIGNING_PORT=8004 \
	cram $(TEST_OPTS) */*/*/test/*.t ## FIXME: This should invoke the desired test suite; is all-wildcards an anti-pattern?
	@date

test_colored::
	$(PRINT)
	grc -c conf.sdc_test $(MAKE) test
