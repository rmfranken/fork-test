#!/bin/bash

if [ $# -lt 1 ] ;
then
	echo "Usage: $0 name [folder]" >&2
	exit 1
else
	mkdir -p $1
	folder=$1
fi

# Generate ECDSA key pair
openssl ecparam -name prime256v1 -genkey -noout -out ${folder}/private.pem
openssl ec -in ${folder}/private.pem -pubout > ${folder}/public.pem

